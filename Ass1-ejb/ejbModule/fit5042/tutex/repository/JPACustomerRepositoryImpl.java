package fit5042.tutex.repository;
import fit5042.tutex.repository.entities.Contact;
import fit5042.tutex.repository.entities.Customer;
import fit5042.tutex.repository.entities.Groups;
import fit5042.tutex.repository.entities.IndustryType;
import fit5042.tutex.repository.entities.Users;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


@Stateless
public class JPACustomerRepositoryImpl implements CustomerRepository  {
	
	@PersistenceContext(unitName = "Ass-ejbPU")
    private EntityManager entityManager;

	@Override
	public void addContact(Contact contact) throws Exception {
		   List<Contact> contacts = entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
		   int maxid = 0;
		   for(Contact tempContact:contacts) {
			   if(tempContact.getContactId() >= maxid) {
				   maxid = tempContact.getContactId();
			   }
		   }
		   contact.setContactId(maxid+ 1);
	        entityManager.persist(contact);
		
	}
	
	@Override
	public void addCustomer(Customer customer) throws Exception {
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		int maxid = 0;
		   for(Customer tempCustomer:customers) {
			   if(tempCustomer.getCustomerId() >= maxid) {
				   maxid = tempCustomer.getCustomerId();
			   }
		   }
		   customer.setCustomerId(maxid+ 1);
	        entityManager.persist(customer);
	}
	
	
	@Override
	public void addUser(Users user) throws Exception {
		List<Users> users = entityManager.createNamedQuery(Users.GET_ALL_QUERY_NAME).getResultList();
		int maxid = 0;
		   for(Users tempUser:users) {
			   if(tempUser.getUserId() >= maxid) {
				   maxid = tempUser.getUserId();
			   }
		   }
		   user.setUserId(maxid+ 1);
	        entityManager.persist(user);
		
	}
	
	@Override
	public void addGroup(Groups group) throws Exception {
		group.setId(getMaxGroupId());
		entityManager.persist(group);
		
	}
	
	@Override
	public void addIndustry(IndustryType industryType) throws Exception {
		List<IndustryType> industryTypes = entityManager.createNamedQuery(IndustryType.GET_ALL_QUERY_NAME).getResultList();
		int maxid = 0;
		   for(IndustryType tempType:industryTypes) {
			   if(tempType.getIndustryId() >= maxid) {
				   maxid = tempType.getIndustryId();
			   }
		   }
		   industryType.setIndustryId(maxid+ 1);
	        entityManager.persist(industryType);
		
	}

	@Override
	public int getMaxGroupId() throws Exception {
		List<Groups> groups = entityManager.createNamedQuery(Groups.GET_ALL_QUERY_NAME).getResultList();
		int maxid = 0;
		   for(Groups group:groups) {
			   if(group.getId() >= maxid) {
				   maxid = group.getId() ;
			   }
		   }
		   maxid = maxid + 1;
		   return maxid;
	}



	@Override
	public Contact searchContactById(int id) throws Exception {
		Contact contact = entityManager.find(Contact.class, id);
        contact.getTags();
        return contact;
		
	}
	

	@Override
	public Customer searchCustomerById(int id) throws Exception {
		Customer customer = entityManager.find(Customer.class, id);  
        return customer;
	}


	@Override
	public List<Contact> getAllContacts() throws Exception {
		
		return entityManager.createNamedQuery(Contact.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
		
	}
	
	@Override
	public List<Users> getAllUsers() throws Exception {
		return entityManager.createNamedQuery(Users.GET_ALL_QUERY_NAME).getResultList();
	}
	
	@Override
	public List<IndustryType> getAllIndustry() throws Exception {
		return entityManager.createNamedQuery(IndustryType.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void removeContact(int contactId) throws Exception {
		
		Contact contact = this.searchContactById(contactId);
		
        if (contact != null) {
            entityManager.remove(contact);
        }
	}
	
	@Override
	public void removeCustomer(int customerId) throws Exception {
		Customer customer = this.searchCustomerById(customerId);
		
        if (customer != null) {
            entityManager.remove(customer);
        }
		
	}

	@Override
	public void removeUser(int userId) throws Exception {
		Users user = entityManager.find(Users.class, userId);
		
		if (user != null) {
            entityManager.remove(user);
        }
		
	}
	@Override
	public void editContact(Contact contact) throws Exception {
		 try {
	            entityManager.merge(contact);
	        } catch (Exception ex) {

	        }
	}
	
	@Override
	public void editCustomer(Customer customer) throws Exception {
		 try {
	            entityManager.merge(customer);
	        } catch (Exception ex) {

	        }
		
	}
	
	@Override
	public void editUser(Users user) throws Exception {
		 try {
	            entityManager.merge(user);
	        } catch (Exception ex) {

	        }
		
	}
	
	@Override
	public void editIndustry(IndustryType industryType) throws Exception {
		 try {
	            entityManager.merge(industryType);
	        } catch (Exception ex) {

	        }
		
	}


	@Override
	public Set<Contact> searchContactByCustomer(Customer customer) throws Exception {
		customer = entityManager.find(Customer.class, customer.getCustomerId());
		customer.getContacts().size();
		
        entityManager.refresh(customer);

        return customer.getContacts();

}

	@Override
	public Users searchUserByName(String userName) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(Users.class);
        
        Root<Users> p = query.from(Users.class);
        
        query.select(p).where(builder.equal(p.get("username").as(String.class), userName));
        
        Users u = (Users)(entityManager.createQuery(query).getResultList().get(0));
        return u;
        
	}

	@Override
	public Set<Customer> searchCustomerByUser(Users user) throws Exception {
		user = entityManager.find(Users.class, user.getUserId());
		user.getCustomers().size();
		
        entityManager.refresh(user);

        return user.getCustomers();
	}




















}
