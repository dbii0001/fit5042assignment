package fit5042.tutex.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import fit5042.tutex.repository.entities.Contact;
import fit5042.tutex.repository.entities.IndustryType;
import fit5042.tutex.repository.entities.Users;
import fit5042.tutex.mbeans.CustomerManagedBean;
import fit5042.tutex.repository.entities.Address;
// Deke Bi 2020/11/02
@RequestScoped
@Named(value = "customer")
public class Customer implements Serializable {
	
	@ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;
	
	private int customerId;
	@NotNull(message = "Customer name can not be empty")
    private String name;
	@NotNull(message = "Country can not be empty")
    private String country;
   
//    private String typeOfIndustry;
   
    private String website;
    @NotNull(message = "Founding date can not be empty")
    @Pattern(regexp="([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))",message="invalid date")
    private String foundingDate;
    @NotNull
    private String introduction;
    @NotNull(message = " Contact name can not be empty")
    private String contactName;
    @NotNull(message = "title can not be empty")
    private String title;
    @NotNull(message = "phone number can not be empty")
    private String mobile;
    @NotNull(message = "email can not be empty")
	 @Pattern(regexp="^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "invalid email")
    private String email;
    
    private Users user;
    private IndustryType industryType;
    
    //Address
    private Address address;
    
    private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private String state;
    
    
    private Set<Contact> contacts;
    
    private Set<String> tags;
    public Customer() {
        this.tags = new HashSet<>();
    }
    
    private Set<fit5042.tutex.repository.entities.Customer> customers;
    
   


	public Customer(int customerId, String name, String country, Address address, String website,
			String foundingDate, String introduction, String contactName, String title, String mobile, String email,
			Set<Contact> contacts, Set<fit5042.tutex.repository.entities.Customer> customers,Users user,IndustryType industryType) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.country = country;
		this.address = address;
//		this.typeOfIndustry = typeOfIndustry;
		this.website = website;
		this.foundingDate = foundingDate;
		this.introduction = introduction;
		this.contactName = contactName;
		this.title = title;
		this.mobile = mobile;
		this.email = email;
		this.contacts = contacts;
		this.customers = customers;
		this.user = user;
		this.industryType = industryType;
	}



	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

//	public String getTypeOfIndustry() {
//		return typeOfIndustry;
//	}
//
//	public void setTypeOfIndustry(String typeOfIndustry) {
//		this.typeOfIndustry = typeOfIndustry;
//	}

	public String getStreetNumber() {
		return streetNumber;
	}



	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}



	public String getStreetAddress() {
		return streetAddress;
	}



	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}



	public String getSuburb() {
		return suburb;
	}



	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}



	public String getPostcode() {
		return postcode;
	}



	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}



	public String getState() {
		return state;
	}



	public void setState(String state) {
		this.state = state;
	}



	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getFoundingDate() {
		return foundingDate;
	}

	public void setFoundingDate(String foundingDate) {
		this.foundingDate = foundingDate;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public Set<fit5042.tutex.repository.entities.Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<fit5042.tutex.repository.entities.Customer> customers) {
		this.customers = customers;
	}

	
	
	public Users getUser() {
		return user;
	}



	public void setUser(Users user) {
		this.user = user;
	}


	public IndustryType getIndustryType() {
		return industryType;
	}



	public void setIndustryType(IndustryType industryType) {
		this.industryType = industryType;
	}



	public Set<String> getTags() {
		return tags;
	}



	public void setTags(Set<String> tags) {
		this.tags = tags;
	}



	public String getContactName() {
		return contactName;
	}



	public void setContactName(String contactName) {
		this.contactName = contactName;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getMobile() {
		return mobile;
	}



	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}




	
	



	
    
    
}
