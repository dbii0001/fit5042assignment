package fit5042.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.tutex.mbeans.CustomerManagedBean;
import javax.faces.bean.ManagedProperty;


@RequestScoped
@Named("removeCustomer")
public class RemoveCustomer {

	
	 @ManagedProperty(value = "#{customerManagedBean}")
	    CustomerManagedBean customerManagedBean;

	    private boolean showForm = true;
	    
	    

	    CustomerApplication app;
	    
	    public boolean isShowForm() {
	        return showForm;
	    }

	    public RemoveCustomer() {
	        ELContext context
	                = FacesContext.getCurrentInstance().getELContext();

	        app = (CustomerApplication) FacesContext.getCurrentInstance()
	                .getApplication()
	                .getELResolver()
	                .getValue(context, null, "customerApplication");

	        

	        //instantiate propertyManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                .getELResolver().getValue(elContext, null, "customerManagedBean");
	    }
	    
	    public void removeCustomer(int customerId) {
	        try {
	            //remove this property from db via EJB
	        	
	        	customerManagedBean.removeCustomer(customerId);
	            //refresh the list in PropertyApplication bean
	            app.searchAll();


	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Property has been deleted succesfully"));
	        } catch (Exception ex) {

	        }
	        showForm = true;

	    }
	    
}
