package fit5042.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.tutex.repository.entities.Customer;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	
	private boolean showForm = true;
	
	private Customer customer;
	
	CustomerApplication app;
	
	private int industryTypeId;
	private String country;
	
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public CustomerApplication getApp() {
		return app;
	}
	public void setApp(CustomerApplication app) {
		this.app = app;
	}
	public int getIndustryTypeId() {
		return industryTypeId;
	}
	public void setIndustryTypeId(int industryTypeId) {
		this.industryTypeId = industryTypeId;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
	public SearchCustomer() {
		
		 ELContext context
         = FacesContext.getCurrentInstance().getELContext();

		 app = (CustomerApplication) FacesContext.getCurrentInstance()
         .getApplication()
         .getELResolver()
         .getValue(context, null, "customerApplication");
	
		 app.updateCustomerList();
	
	}
	
	public void searchCustomerByCountryAndIndustry(String country,int industryId) {
		try {
			app.searchCustomerByCountryAndIndustry(country, industryId);
		}
		catch(Exception ex){
			
		}
	}
	
	public void searchAll() {
		try {
			app.searchAll();
		}
		catch(Exception ex){
			
		}
	}

}
