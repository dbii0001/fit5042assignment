package fit5042.tutex.controllers;
import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.tutex.mbeans.CustomerManagedBean;

import javax.faces.bean.ManagedProperty;

@RequestScoped
@Named("addIndustry")
public class AddIndustry {

	
	
	 @ManagedProperty(value = "#{customerManagedBean}")
	 CustomerManagedBean customerManagedBean;

	    private boolean showForm = true;
	    
	    CustomerApplication app;
	    
	    public boolean isShowForm() {
	        return showForm;
	    }
	    
	    
	    public AddIndustry() {
	    	ELContext context
            = FacesContext.getCurrentInstance().getELContext();

	    	app = (CustomerApplication) FacesContext.getCurrentInstance()
            .getApplication()
            .getELResolver()
            .getValue(context, null, "customerApplication");

	    	//instantiate propertyManagedBean
	    	ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	    	customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
            .getELResolver().getValue(elContext, null, "customerManagedBean");
	    	
	    }
	    
	    public void addIndustry(Industry localIndustry) {
	        //this is the local property, not the entity
	        try {
	            //add this property to db via EJB
	        	String typename = localIndustry.getTypeName() ;
	        	
	        	if(typename.equals("Bank") || 
	        		typename.equals("Building") || 
	        		typename.equals("Data Communication") ||
	        		typename.equals("Education") ||
	        		typename.equals("Farm") ||
	        		typename.equals("Health") ||
	        		typename.equals("Mining") ||
	        		typename.equals("Publishing") ) 
	        	{
	              	customerManagedBean.addIndustry(localIndustry);

		            //refresh the list in PropertyApplication bean
		        	app.searchAllIndustry();
		            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Industry has been added succesfully"));
	        	}
	        	
	        	else {
	        		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Industry Type must be from Bank,Building,Data Communication,"
	        				+ "Education,Farm,Health,Mining,Publishing"));
	        	}
	  
	        } catch (Exception ex) {

	        }
	        showForm = true;
	    }
}
