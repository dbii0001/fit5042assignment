package fit5042.tutex.controllers;
import java.util.ArrayList;
import javax.el.ELContext;

import fit5042.tutex.mbeans.CustomerManagedBean;
import fit5042.tutex.repository.entities.Contact;
import fit5042.tutex.repository.entities.Customer;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

@Named(value = "contactController")
@RequestScoped
public class ContactController {
	 @ManagedProperty(value = "#{customerManagedBean}")
	    CustomerManagedBean customerManagedBean;
	 
	 private boolean showForm = true;
 
	 private int contactId; //this propertyId is the index, don't confuse with the Property Id
 
 CustomerApplication app;

 public int getContactId() {
     return contactId;
 }

 public void setContactId(int contactId) {
     this.contactId = contactId;
 }
 
 
 public CustomerApplication getCustomerApplication() {
		return app;
	}

	public void setCustomerApplication(CustomerApplication customerApplication) {
		this.app = customerApplication;
	}


	private fit5042.tutex.repository.entities.Contact contact;
	
	public ContactController() {
        // Assign property identifier via GET param 
        //this propertyID is the index, don't confuse with the Property Id
    	 ELContext context
         = FacesContext.getCurrentInstance().getELContext();
 app = (CustomerApplication) FacesContext.getCurrentInstance()
                 .getApplication()
                 .getELResolver()
                 .getValue(context, null, "customerApplication");
    	
    	contactId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactID"));
        // Assign property based on the id provided 
    	contact = getContact();
     
       
    }
	
	 public fit5042.tutex.repository.entities.Contact getContact() {
	        if (contact == null) {
	            // Get application context bean PropertyApplication 
	           
	            // -1 to propertyId since we +1 in JSF (to always have positive property id!) 
	            return app.getContacts().get(--contactId); //this propertyId is the index, don't confuse with the Property Id
	        }
	        return contact;
	        
	        
	    }

}
