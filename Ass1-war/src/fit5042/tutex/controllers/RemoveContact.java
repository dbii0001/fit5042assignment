package fit5042.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.tutex.mbeans.CustomerManagedBean;
import javax.faces.bean.ManagedProperty;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Guan
 */
@RequestScoped
@Named("removeContact")
public class RemoveContact {

    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    private boolean showForm = true;
    
    

    CustomerApplication app;

    public boolean isShowForm() {
        return showForm;
    }

    public RemoveContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerApplication");

        

        //instantiate propertyManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
    }

    /**
     * @param property Id
     */
    public void removeContact(int contactId) {
        try {
            //remove this property from db via EJB
        	
        	customerManagedBean.removeContact(contactId);
            //refresh the list in PropertyApplication bean
           

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Property has been deleted succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;

    }

}
