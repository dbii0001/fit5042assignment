package fit5042.tutex.controllers;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import fit5042.tutex.repository.entities.Customer;

@RequestScoped
@Named(value = "industry")
public class Industry {

	private int industryId;
	@NotNull
	private String typeName;
	
	private Set<Customer> customers;
	
	public Industry() {}

	public Industry(int industryId, String typeName, Set<Customer> customers) {
		super();
		this.industryId = industryId;
		this.typeName = typeName;
		this.customers = customers;
	}

	public int getIndustryId() {
		return industryId;
	}

	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	@Override
	public String toString() {
		return "Industry [industryId=" + industryId + ", typeName=" + typeName + ", customers=" + customers + "]";
	}
	
	
			
	
}
