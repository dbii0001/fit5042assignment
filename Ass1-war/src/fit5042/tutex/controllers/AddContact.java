package fit5042.tutex.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.tutex.mbeans.CustomerManagedBean;
import fit5042.tutex.repository.entities.Customer;

import javax.faces.bean.ManagedProperty;

@RequestScoped
@Named("addContact")
public class AddContact {
	@ManagedProperty(value = "#{customerManagedBean}")
	 CustomerManagedBean customerManagedBean;
	
	private boolean showForm = true;
	private Contact contact;
	
	
	
	  public Contact getContact() {
		return contact;
	}



	public void setContact(Contact contact) {
		this.contact = contact;
	}



	public boolean isShowForm() {
	        return showForm;
	    }
	
	
	 public AddContact() {
	        ELContext context
	                = FacesContext.getCurrentInstance().getELContext();



	        //instantiate propertyManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                .getELResolver().getValue(elContext, null, "customerManagedBean");
	    }
	 
	 public void addCustomer(Customer customer,Contact localContact) {
	        //this is the local property, not the entity
	        try {
	            //add this property to db via EJB
	        	customerManagedBean.addContact(customer,localContact);


	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been added succesfully"));
	        } catch (Exception ex) {

	        }
	        showForm = true;
	    }
}
