package fit5042.tutex.controllers.user;
import java.util.ArrayList;
import javax.el.ELContext;

import fit5042.tutex.mbeans.CustomerManagedBean;
import fit5042.tutex.repository.entities.Contact;
import fit5042.tutex.repository.entities.Customer;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

/**
 *
 * @author Messom
 * @author Guan
 */
@Named(value = "userCustomerController")
@RequestScoped
public class CustomerController {
	
	 @ManagedProperty(value = "#{customerManagedBean}")
	    CustomerManagedBean customerManagedBean;
	 
	private boolean showForm = true;
    private int customerId; //this propertyId is the index, don't confuse with the Property Id
    
    UserController app;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
   

	public UserController getApp() {
		return app;
	}

	public void setApp(UserController app) {
		this.app = app;
	}


	private fit5042.tutex.repository.entities.Customer customer;
    private ArrayList<Contact> contactList = new ArrayList<Contact>();

    
    public ArrayList<Contact> getContactList() {
		return contactList;
	}

	public void setContactList(ArrayList<Contact> contactList) {
		this.contactList = contactList;
	}

	public CustomerController() {
        // Assign property identifier via GET param 
        //this propertyID is the index, don't confuse with the Property Id
    	 ELContext context
         = FacesContext.getCurrentInstance().getELContext();
 app = (UserController) FacesContext.getCurrentInstance()
                 .getApplication()
                 .getELResolver()
                 .getValue(context, null, "userController");
    	
    	customerId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerID"));
        // Assign property based on the id provided 
        customer = getCustomer();
        getContacts(customer);
       
    }

    public fit5042.tutex.repository.entities.Customer getCustomer() {
        if (customer == null) {
            // Get application context bean PropertyApplication 
           
            // -1 to propertyId since we +1 in JSF (to always have positive property id!) 
            return app.getCustomerList().get(--customerId); //this propertyId is the index, don't confuse with the Property Id
        }
        return customer;
        
        
    }
    
    public void getContacts(Customer customer){
    	 try {
    	app.searchContactsByCustomer(customer);
    	setContactList(app.getContactList());
    	
    	}catch (Exception ex) {

        }
        showForm = true;
   
    }
}
