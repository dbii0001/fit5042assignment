package fit5042.tutex.controllers.user;
import java.util.ArrayList;
import javax.el.ELContext;

import fit5042.tutex.mbeans.CustomerManagedBean;
import fit5042.tutex.repository.entities.Contact;
import fit5042.tutex.repository.entities.Customer;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

@Named(value = "userContactController")
@RequestScoped
public class ContactController {
	 @ManagedProperty(value = "#{customerManagedBean}")
	    CustomerManagedBean customerManagedBean;
	 
	 private boolean showForm = true;
 
	 private int contactId; //this propertyId is the index, don't confuse with the Property Id
 
	 UserController app;

 public int getContactId() {
     return contactId;
 }

 public void setContactId(int contactId) {
     this.contactId = contactId;
 }
 

public UserController getApp() {
	return app;
}

public void setApp(UserController app) {
	this.app = app;
}


	private fit5042.tutex.repository.entities.Contact contact;
	
	public ContactController() {
        // Assign property identifier via GET param 
        //this propertyID is the index, don't confuse with the Property Id
    	 ELContext context
         = FacesContext.getCurrentInstance().getELContext();
 app = (UserController) FacesContext.getCurrentInstance()
                 .getApplication()
                 .getELResolver()
                 .getValue(context, null, "userController");
    	
    	contactId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("contactID"));
        // Assign property based on the id provided 
    	contact = getContact();
     
       
    }
	
	 public fit5042.tutex.repository.entities.Contact getContact() {
	        if (contact == null) {
	            // Get application context bean PropertyApplication 
	           
	            // -1 to propertyId since we +1 in JSF (to always have positive property id!) 
	            return app.getContactList().get(--contactId); //this propertyId is the index, don't confuse with the Property Id
	        }
	        return contact;
	        
	        
	    }

}
