package fit5042.tutex.controllers.user;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.tutex.mbeans.CustomerManagedBean;
import javax.faces.bean.ManagedProperty;


@RequestScoped
@Named("removeUserCustomer")
public class RemoveCustomer {

	
	 @ManagedProperty(value = "#{customerManagedBean}")
	    CustomerManagedBean customerManagedBean;

	    private boolean showForm = true;
	    
	    

	    UserController app;
	    
	    public boolean isShowForm() {
	        return showForm;
	    }

	    public RemoveCustomer() {
	        ELContext context
	                = FacesContext.getCurrentInstance().getELContext();

	        app = (UserController) FacesContext.getCurrentInstance()
	                .getApplication()
	                .getELResolver()
	                .getValue(context, null, "userController");

	        

	        //instantiate propertyManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                .getELResolver().getValue(elContext, null, "customerManagedBean");
	    }
	    
	    public void removeCustomer(int customerId) {
	        try {
	            //remove this property from db via EJB
	        	
	        	customerManagedBean.removeCustomer(customerId);
	            //refresh the list in PropertyApplication bean
	            app.updateCustomerList(app.getUser());


	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Property has been deleted succesfully"));
	        } catch (Exception ex) {

	        }
	        showForm = true;

	    }
	    
}
