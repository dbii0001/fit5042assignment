package fit5042.tutex.controllers;

import java.util.ArrayList;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import fit5042.tutex.mbeans.CustomerManagedBean;


import javax.inject.Named;
import fit5042.tutex.repository.entities.Customer;
import fit5042.tutex.repository.entities.IndustryType;
import fit5042.tutex.repository.entities.Users;
import fit5042.tutex.repository.entities.Contact;

import javax.el.ELContext;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

@Named(value = "customerApplication")
@ApplicationScoped

public class CustomerApplication {

	 @ManagedProperty(value = "#{customerManagedBean}")
	    CustomerManagedBean customerManagedBean;
	 
	 
	  private ArrayList<Customer> customers;
	  private ArrayList<Contact> contacts;
	  private ArrayList<Users> users;
	  private ArrayList<IndustryType> industryTypes;

	    private boolean showForm = true;

	    public boolean isShowForm() {
	        return showForm;
	    }
	    
	    public CustomerApplication() throws Exception {
	        customers = new ArrayList<>();
	        contacts = new ArrayList<>();
	        users = new ArrayList<>();
	        industryTypes = new ArrayList<>();
	        //instantiate propertyManagedBean
	        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
	        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
	                .getELResolver().getValue(elContext, null, "customerManagedBean");

	        //get properties from db 
	        updateCustomerList();
	        updateUserList();
	        updateIndustryList() ;
	    }
	    
	    
	    
	    public ArrayList<Users> getUsers() {
			return users;
		}

		public void setUsers(ArrayList<Users> users) {
			this.users = users;
		}
		

		public ArrayList<IndustryType> getIndustryTypes() {
			return industryTypes;
		}

		public void setIndustryTypes(ArrayList<IndustryType> industryTypes) {
			this.industryTypes = industryTypes;
		}

		public ArrayList<Customer> getCustomers() {
	        return customers;
	    }

	    private void setCustomers(ArrayList<Customer> newCustomers) {
	        this.customers = newCustomers;
	    }
	    
	    
	    
	    public ArrayList<Contact> getContacts() {
			return contacts;
		}

		public void setContacts(ArrayList<Contact> contacts) {
			this.contacts = contacts;
		}

		public void updateCustomerList() {
	        if (customers != null && customers.size() > 0)
	        {
	            
	        }
	        else
	        {
	            customers.clear();

	            for (fit5042.tutex.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
	            {
	            	customers.add(customer);
	            }

	            setCustomers(customers);
	        }
	    }
		
		public void updateUserList() {
	        if (users != null && users.size() > 0)
	        {
	            
	        }
	        else
	        {
	            users.clear();

	            for (fit5042.tutex.repository.entities.Users user : customerManagedBean.getAllUsers())
	            {
	            	users.add(user);
	            }

	            setUsers(users);
	        }
	    }
		
		public void updateIndustryList() {
	        if (industryTypes != null && industryTypes.size() > 0)
	        {
	            
	        }
	        else
	        {
	        	industryTypes.clear();

	        	 for (fit5042.tutex.repository.entities.IndustryType industryType : customerManagedBean.getAllIndustry())
		            {
		            	industryTypes.add(industryType);
		            }

		            setIndustryTypes(industryTypes);

	            
	        }
	    }
		
		public void searchAllUsers()
	    {
			 users.clear();

	            for (fit5042.tutex.repository.entities.Users user : customerManagedBean.getAllUsers())
	            {
	            	users.add(user);
	            }

	            setUsers(users);
	    }
		
		public void searchAllIndustry()
	    {
			industryTypes.clear();

	            for (fit5042.tutex.repository.entities.IndustryType industryType : customerManagedBean.getAllIndustry())
	            {
	            	industryTypes.add(industryType);
	            }

	            setIndustryTypes(industryTypes);
	    }
	    
	    public void searchAll()
	    {
	    	customers.clear();
	        
	        for (fit5042.tutex.repository.entities.Customer customer : customerManagedBean.getAllCustomers())
	        {
	            customers.add(customer);
	        }
	        
	        setCustomers(customers);
	    }
	    
	    public void searchContactsByCustomer(Customer customer) {
	    	contacts.clear();

	        for (fit5042.tutex.repository.entities.Contact contact : customerManagedBean.searchContactByCustomer(customer)) {
	            contacts.add(contact);
	        }

	        setContacts(contacts);
	    }
	    
	    public void searchCustomerByCountryAndIndustry(String country,int industryId) {
	    	customers.clear();
	    	Set<Customer> filterCustomers = customerManagedBean.searchCustomerByCountryAndIndustry(country, industryId);
	    	
	    	for(Customer customer:filterCustomers) 
	    	{
	    		
	    		customers.add(customer);
;	    	}
	    }
}
