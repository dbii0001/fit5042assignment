package fit5042.tutex.controllers;

import java.util.ArrayList;
import javax.el.ELContext;

import fit5042.tutex.mbeans.CustomerManagedBean;
import fit5042.tutex.repository.entities.Contact;
import fit5042.tutex.repository.entities.Customer;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;


@Named(value = "industryController")
@RequestScoped
public class IndustryController {

	 @ManagedProperty(value = "#{customerManagedBean}")
	    CustomerManagedBean customerManagedBean;
	 
	 	private boolean showForm = true;
	 	
	 	private int industryId;//index
	 	
	 	CustomerApplication app;

		public int getIndustryId() {
			return industryId;
		}

		public void setIndustryId(int industryId) {
			this.industryId = industryId;
		}

		public CustomerApplication getApp() {
			return app;
		}

		public void setApp(CustomerApplication app) {
			this.app = app;
		}
	 	
		private fit5042.tutex.repository.entities.IndustryType industryType;
		
		public IndustryController() {
	        // Assign property identifier via GET param 
	        //this propertyID is the index, don't confuse with the Property Id
	    	 ELContext context
	         = FacesContext.getCurrentInstance().getELContext();
	 app = (CustomerApplication) FacesContext.getCurrentInstance()
	                 .getApplication()
	                 .getELResolver()
	                 .getValue(context, null, "customerApplication");
	    	
	 industryId = Integer.valueOf(FacesContext.getCurrentInstance()
	                .getExternalContext()
	                .getRequestParameterMap()
	                .get("industryID"));
	        // Assign property based on the id provided 
	 industryType = getIndustry();
	     
	       
	    }
		
		 public fit5042.tutex.repository.entities.IndustryType getIndustry() {
		        if (industryType == null) {
		            // Get application context bean PropertyApplication 
		           
		            // -1 to propertyId since we +1 in JSF (to always have positive property id!) 
		            return app.getIndustryTypes().get(--industryId); //this propertyId is the index, don't confuse with the Property Id
		        }
		        return industryType;
		        
		        
		    }
	 
}
