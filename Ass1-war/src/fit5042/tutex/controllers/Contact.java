package fit5042.tutex.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import fit5042.tutex.repository.entities.Customer;


@RequestScoped
@Named(value = "contact")
public class Contact implements Serializable{
	 private int contactId;
	 @NotNull(message = " Contact name can not be empty")
	 private String name;
	 @NotNull(message = "title can not be empty")
	 private String title;
	 @NotNull(message = "phone number can not be empty")
	  private String mobilePhone;
	 @NotNull(message = "email can not be empty")
	 @Pattern(regexp="^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", message = "invalid email")
	    private String email;
	    
	    private Customer customer;

	    private Set<String> tags;

	    public Contact() {
	        this.tags = new HashSet<>();
	    }
	    public Contact(int contactId, String name, String title, String mobilePhone, String email, Customer customer,
				Set<String> tags) {
			this.contactId = contactId;
			this.name = name;
			this.title = title;
			this.mobilePhone = mobilePhone;
			this.email = email;
			this.customer = customer;
			this.tags = tags;
		}
		public int getContactId() {
			return contactId;
		}
		public void setContactId(int contactId) {
			this.contactId = contactId;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getMobilePhone() {
			return mobilePhone;
		}
		public void setMobilePhone(String mobilePhone) {
			this.mobilePhone = mobilePhone;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public Customer getCustomer() {
			return customer;
		}
		public void setCustomer(Customer customer) {
			this.customer = customer;
		}
		public Set<String> getTags() {
			return tags;
		}
		public void setTags(Set<String> tags) {
			this.tags = tags;
		}
		@Override
		public String toString() {
			return "Contact [contactId=" + contactId + ", name=" + name + ", title=" + title + ", mobilePhone="
					+ mobilePhone + ", email=" + email + ", customer=" + customer + ", tags=" + tags + "]";
		}
	    
		
	    
}
