package fit5042.tutex.mbeans;
import fit5042.tutex.repository.CustomerRepository;
import fit5042.tutex.repository.entities.Address;
import fit5042.tutex.repository.entities.Contact;

import fit5042.tutex.repository.entities.Customer;
import fit5042.tutex.repository.entities.Groups;
import fit5042.tutex.repository.entities.IndustryType;
import fit5042.tutex.repository.entities.Users;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
	
	@EJB
	CustomerRepository customerRepository;
	
	private String checkPsw;
	
	
	
	
	public CustomerManagedBean() {
    }
	
	public String getCheckPsw() {
		return checkPsw;
	}

	public void setCheckPsw(String checkPsw) {
		this.checkPsw = checkPsw;
	}
	
	
	
	 	public Users searchUserByName(String userName) {
	        try {
	            Users user = customerRepository.searchUserByName(userName);
	            return user;
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        return null;
	    }
	
	
	 	public List<Users> getAllUsers() {
		        try {
		            List<Users> users = customerRepository.getAllUsers();
		            return users;
		        } catch (Exception ex) {
		            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		        }
		        return null;
		    }
	 	
	 	public List<IndustryType> getAllIndustry() {
	        try {
	            List<IndustryType> industryTypes = customerRepository.getAllIndustry();
	            return industryTypes;
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        return null;
	    }
	 	
	 	
	 	
	   public List<Customer> getAllCustomers() {
	        try {
	            List<Customer> customers = customerRepository.getAllCustomers();
	            return customers;
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        return null;
	    }
	
	   public void addCustomer(fit5042.tutex.controllers.Customer localCustomer) {
	        //convert this newProperty which is the local property to entity property
	        Customer customer = convertCustomerToEntity(localCustomer);
	        Users user = new Users();
	        user = localCustomer.getUser();
	        IndustryType industryType = new IndustryType();
	        industryType = localCustomer.getIndustryType();
	        
	        if (user.getUserId() == 0) {
	        	user = null;
	        }
	        
	        customer.setUser(user);
	        
	        if (industryType.getIndustryId() == 0) {
	        	industryType = null;
	        }
	        
	        customer.setIndustryType(industryType);

	        try {
	            customerRepository.addCustomer(customer);
	            
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	 
	    }
	   
	   public void addUserCustomer(fit5042.tutex.controllers.Customer localCustomer,fit5042.tutex.repository.entities.Users user) {
	        //convert this newProperty which is the local property to entity property
	        Customer customer = convertCustomerToEntity(localCustomer);
	        customer.setUser(user);
	        IndustryType industryType = new IndustryType();
	        industryType = localCustomer.getIndustryType();
	        
	        if (industryType.getIndustryId() == 0) {
	        	industryType = null;
	        }
	        
	        customer.setIndustryType(industryType);


	        try {
	            customerRepository.addCustomer(customer);
	            
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
	        }
	        
	 
	    }
	   
	   public void addContact(Customer customer,fit5042.tutex.controllers.Contact localContact) {
		   Contact contact = convertContactToEntity(customer,localContact);
		   try {
			   customerRepository.addContact(contact);
		   
		   }catch (Exception ex) {
	            Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
	        }
		   
	   }
	   
	   
	   public void addUser(fit5042.tutex.controllers.User localUser) {
		   Users user = convertUserToEntity(localUser);
		   Groups group = new Groups();
		   group.setUsername(user.getUsername());
		   group.setGroupname("user");
		   try {
			   customerRepository.addUser(user);
			   customerRepository.addGroup(group);
			   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User has been added succesfully"));
		   }catch (Exception ex) {
			   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("This user name is existing,please enter other user name"));
	            Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
	        }
		   
	   }
	   
	   public void addIndustry(fit5042.tutex.controllers.Industry localIndustry) {
		
		   IndustryType industryType = new IndustryType();
		   industryType.setTypeName(localIndustry.getTypeName());
		   
		   try {
			   customerRepository.addIndustry(industryType);
		   
		   }catch (Exception ex) {
	            Logger.getLogger(CustomerRepository.class.getName()).log(Level.SEVERE, null, ex);
	        }
		   
	   }
	   
	   

	   
	   public Set<Contact> searchContactByCustomer(Customer customer) {
	        try {
	            return customerRepository.searchContactByCustomer(customer);
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }

	        return null;
	    }
	   
	   public Set<Customer> searchCustomerByUser(Users user) {
	        try {
	            return customerRepository.searchCustomerByUser(user);
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }

	        return null;
	    }
	   
	   public Set<Customer> searchCustomerByCountryAndIndustry(String country,int industryId){
		   
		   Set<Customer> filterCustomer = new HashSet<>();
		   try {
			   for (Customer customer: customerRepository.getAllCustomers())
			   {
				   if(customer.getCountry().equals(country) && customer.getIndustryType().getIndustryId() == industryId) 
				   {
					   filterCustomer.add(customer);
				   }
			   }
			   return filterCustomer;
		   }
		   catch(Exception ex){
			   
		   }
		   
		   return null;
	   }
	   
	   
	   
	    public void editContact(Contact contact) {
	        try {

	            customerRepository.editContact(contact);

	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been updated succesfully"));
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	    
	    public void editCustomer(Customer customer) {
	        try {

	            customerRepository.editCustomer(customer);

	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	    
	    public void editUser(Users user,String localPassword) {
	    	String password = localPassword;
	    	
	    	try {
	    		String hashPassword = convertPasswordToSha256(password);
	    		user.setPassword(hashPassword);

	            customerRepository.editUser(user);

	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Password has been updated succesfully"));
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	    
	    public void editIndustry(IndustryType industryType ) {
	        try {

	        	String typename = industryType.getTypeName();
	        	if(typename.equals("Bank") || 
		        		typename.equals("Building") || 
		        		typename.equals("Data Communication") ||
		        		typename.equals("Education") ||
		        		typename.equals("Farm") ||
		        		typename.equals("Health") ||
		        		typename.equals("Mining") ||
		        		typename.equals("Publishing") ) 
		        	{
		        	 customerRepository.editIndustry(industryType);

			            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Industry has been updated succesfully"));
		        	}
		        	
		        	else {
		        		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Industry Type must be from Bank,Building,Data Communication,"
		        				+ "Education,Farm,Health,Mining,Publishing"));
		        	}

	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	    
	   
	   public void removeContact(int contactId) {
	        try {
	        	customerRepository.removeContact(contactId);
	 
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	   
	   public void removeCustomer(int customerId) {
	        try {
	        	customerRepository.removeCustomer(customerId);
	 
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	   
	   public void removeUser(int userId) {
	        try {
	        	customerRepository.removeUser(userId);
	 
	        } catch (Exception ex) {
	            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
	        }
	    }
	   
	   
	   public String checkPasswordCorrect(String text) {
		   fit5042.tutex.repository.entities.Users tempUser = searchUserByName(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
		   String Password = tempUser.getPassword();
		   
		   
	
		  if( Password == checkPsw ) {
			
			  return "Correct password input";
		  }
		  else {
			 
			  return "InCorrect password input";
		  }
		  
		  
	  }
	   
	   
	   public String convertPasswordToSha256(String password) throws NoSuchAlgorithmException {
		   MessageDigest md = MessageDigest.getInstance("SHA-256");
		   md.update(password.getBytes());
		   
		   byte[] digest = md.digest();
		   StringBuffer sb = new StringBuffer();
		   for(byte b : digest) {
			   sb.append(String.format("%02x", b & 0xff));
		   }
		   
		   return sb.toString();
	   }
	   
	   private Customer convertCustomerToEntity(fit5042.tutex.controllers.Customer localCustomer) {
		   	Customer customer = new Customer(); //entity
	        
		   	String name = localCustomer.getName();
		    String country = localCustomer.getCountry();
		    String streetNumber = localCustomer.getStreetNumber();
	        String streetAddress = localCustomer.getStreetAddress();
	        String suburb = localCustomer.getSuburb();
	        String postcode = localCustomer.getPostcode();
	        String state = localCustomer.getState();
		    Address address = new Address(streetNumber,streetAddress,suburb,postcode,state);

		    String website = localCustomer.getWebsite();
		    String foundingDate = localCustomer.getFoundingDate();
		    String introduction = localCustomer.getIntroduction();
		    
	       
		    customer.setName(name);
	        customer.setCountry(country);
	        customer.setAddress(address);

	        customer.setWebsite(website);
	        customer.setFoundingDate(foundingDate);
	        customer.setIntroduction(introduction);
	        Set<Contact> contacts = new HashSet<>();

//	        Contact contact = new Contact();
//	        
//	        String contactName = localCustomer.getContactName();
//	        String title = localCustomer.getTitle();
//	        String mobile = localCustomer.getMobile();
//	        String email = localCustomer.getEmail();
//	        
//	        contact.setName(contactName);
//	        contact.setTitle(title);
//	        contact.setMobilePhone(mobile);
//	        contact.setEmail(email);
	        
//	        contact.setCustomer(customer);
//	        contacts.add(contact);
	        
	        customer.setContacts(contacts);
	        
	        
	        
	        return customer;
	    }
	   
	   private Contact convertContactToEntity(Customer customer,fit5042.tutex.controllers.Contact localContact) {
		   	Contact contact = new Contact(); //entity
	        
		   	String name = localContact.getName();
		   	String title = localContact.getTitle();
		   	String mobile = localContact.getMobilePhone();
		   	String email= localContact.getEmail();
		   	contact.setName(name);
		   	contact.setTitle(title);
		   	contact.setMobilePhone(mobile);
		   	contact.setEmail(email);
		   	contact.setTags(localContact.getTags());
		   	contact.setCustomer(customer);
	       

	        
	        return contact;
	    }
	   
	   private Users convertUserToEntity(fit5042.tutex.controllers.User localUser) {
		   	Users user = new Users(); //entity
	        
		   	String name = localUser.getUsername();
		   	String password = localUser.getPassword();
		   	String hashPassword = "";
		   	
		   	try {
	    		hashPassword = convertPasswordToSha256(password);
	    
	        } catch (Exception ex) {
	        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Password 256 error"));
	        }
	       
		   	user.setUsername(name);
		   	user.setPassword(hashPassword);

		   	Set<Customer> customers = new HashSet<>();
		   	user.setCustomers(customers);
		   	
		   	return user;
	    }
	   
	   
	   
	   
	   
}
