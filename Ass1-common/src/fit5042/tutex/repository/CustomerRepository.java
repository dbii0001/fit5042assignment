package fit5042.tutex.repository;

import java.util.List;
import java.util.Set;
import javax.ejb.Remote;

import fit5042.tutex.repository.entities.Contact;
import fit5042.tutex.repository.entities.Customer;
import fit5042.tutex.repository.entities.Users;
import fit5042.tutex.repository.entities.Groups;
import fit5042.tutex.repository.entities.IndustryType;
@Remote
public interface CustomerRepository {

    public void addContact(Contact contact) throws Exception;
    
    public void addCustomer(Customer customer) throws Exception;
    
    public void addUser(Users user) throws Exception;
    
    public void addGroup(Groups group) throws Exception;
    
    public void addIndustry(IndustryType industryType) throws Exception;
    
    public int getMaxGroupId()throws Exception;

    public Contact searchContactById(int id) throws Exception;
    public Customer searchCustomerById(int id) throws Exception;

    public List<Contact> getAllContacts() throws Exception;

    public List<Customer> getAllCustomers() throws Exception;
    
    public List<Users> getAllUsers() throws Exception;
    
    public List<IndustryType> getAllIndustry() throws Exception;

    public void removeContact(int contactId) throws Exception;
    public void removeCustomer(int customerId) throws Exception;
    public void removeUser(int userId) throws Exception;

    public void editContact(Contact contact) throws Exception;
    public void editCustomer(Customer customer) throws Exception;
    public void editUser(Users user) throws Exception;
    public void editIndustry(IndustryType industryType) throws Exception;
    
    public Users searchUserByName(String userName) throws Exception;

    Set<Contact> searchContactByCustomer(Customer customer) throws Exception;
    Set<Customer> searchCustomerByUser(Users user) throws Exception;
} 
