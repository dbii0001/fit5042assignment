package fit5042.tutex.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "IndustryType")
@NamedQueries({
    @NamedQuery(name = IndustryType.GET_ALL_QUERY_NAME, query = "SELECT i FROM IndustryType i")})
public class IndustryType implements Serializable {
	
	public static final String GET_ALL_QUERY_NAME = "IndustryType.getAll";
	
	private int industryId;
	private String typeName;
	
	private Set<Customer> customers;
	
	public IndustryType() {
		
	}

	public IndustryType(int industryId, String typeName, Set<Customer> customers) {
		super();
		this.industryId = industryId;
		this.typeName = typeName;
		this.customers = new HashSet<>();
	}

	@Id
    @GeneratedValue
	public int getIndustryId() {
		return industryId;
	}

	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@OneToMany(mappedBy = "industryType")
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + industryId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndustryType other = (IndustryType) obj;
		if (industryId != other.industryId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IndustryType [industryId=" + industryId + ", typeName=" + typeName + ", customers=" + customers + "]";
	}


	

	
}
