package fit5042.tutex.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Customer")
@NamedQueries({
    @NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c")})
public class Customer implements Serializable {

	public static final String GET_ALL_QUERY_NAME = "Customer.getAll";

    private int customerId;
    private String name;
    private String country;
    private Address address;
//    private String address;
//    private String typeOfIndustry;
    private String website;
    private String foundingDate;
    private String introduction;
    
    
    private Set<Contact> contacts;
    
    private Users user;
    private IndustryType industryType;

    private Set<String> tags;
    

    public Customer() {
        this.tags = new HashSet<>();
    }


    public Customer(int customerId, String name, String country, Address address,String website,
			String foundingDate, String introduction,Users user,IndustryType industryType,Set<String> tags) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.country = country;
		this.address = address;
//		this.typeOfIndustry = typeOfIndustry;
		this.website = website;
		this.foundingDate = foundingDate;
		this.introduction = introduction;
		this.user = user;
		this.industryType = industryType;
		this.tags = tags;
		this.contacts = new HashSet<>();
	}
    
    

	@Id
    @GeneratedValue
    @Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	@Embedded
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

//	@Column(name = "industry_type")
//	public String getTypeOfIndustry() {
//		return typeOfIndustry;
//	}
//
//	public void setTypeOfIndustry(String typeOfIndustry) {
//		this.typeOfIndustry = typeOfIndustry;
//	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Column(name = "founding_date")
	public String getFoundingDate() {
		return foundingDate;
	}

	public void setFoundingDate(String foundingDate) {
		this.foundingDate = foundingDate;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	//enforce the relationship between a property and its contact person using annotation(s). Each property has one and only one contact person. Each contact person might be responsible for zero to many properties
    @OneToMany(mappedBy = "customer",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}
	
	@ManyToOne
	
	public Users getUser() {
		return user;
	}


	public void setUser(Users user) {
		this.user = user;
	}
	
	@ManyToOne
	public IndustryType getIndustryType() {
		return industryType;
	}


	public void setIndustryType(IndustryType industryType) {
		this.industryType = industryType;
	}


	@ElementCollection
    @CollectionTable(name = "CustomerTag")
    @Column(name = "value")
    public Set<String> getTags() {
        return tags;
    }

	public void setTags(Set<String> tags) {
        this.tags = tags;
    }


	public void addContact(Contact contact)
	{
		contact.setCustomer(this);
		contacts.add(contact);
	}
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.customerId;
        return hash;
    }


	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (this.customerId != other.customerId) {
            return false;
        }
        return true;
    }


	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name + ", country=" + country + ", address=" + address
				+ ", website=" + website + ", foundingDate=" + foundingDate + ", introduction=" + introduction
				+ ", contacts=" + contacts + ", user=" + user + ", industryType=" + industryType + ", tags=" + tags
				+ "]";
	}








}
