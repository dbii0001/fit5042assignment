package fit5042.tutex.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Deke Bi
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Contact.GET_ALL_QUERY_NAME, query = "SELECT c FROM Contact c order by c.contactId desc")})

public class Contact implements Serializable {
	
	public static final String GET_ALL_QUERY_NAME = "Contact.getAll";

    private int contactId;
    private String name;
    private String title;
    private String mobilePhone;
    private String email;
    
    private Customer customer;

    private Set<String> tags;

    public Contact() {
        this.tags = new HashSet<>();
    }



    public Contact(int contactId, String name, String title, String mobilePhone, String email, Customer customer,
			Set<String> tags) {
		this.contactId = contactId;
		this.name = name;
		this.title = title;
		this.mobilePhone = mobilePhone;
		this.email = email;
		this.customer = customer;
		this.tags = tags;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "contact_id")
	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	@Column(name = "mobile_Phone")
	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
    @ManyToOne
    public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

    //annotate the attribute tags in Property class so that the tags of a property will be stored in a table called TAG. 
    //The tags of a property should be eagerly fetched and the value of each tag must be stored in a column VALUE in the TAG table
    @ElementCollection
    @CollectionTable(name = "contactTag")
    @Column(name = "value")
    public Set<String> getTags() {
        return tags;
    }

	public void setTags(Set<String> tags) {
        this.tags = tags;
    }



	@Override
	public String toString() {
		return "Contact [contactId=" + contactId + ", name=" + name + ", title=" + title + ", mobilePhone="
				+ mobilePhone + ", email=" + email + ", customer=" + customer + ", tags=" + tags + "]";
	}


	

}
